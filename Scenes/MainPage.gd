extends Control

onready var initial_page = preload("res://Scenes/UIElements/content/InitialPage/initialPage.tscn")

onready var sections = $Sections/VBoxContainer/ColorRect/VBoxContainer
onready var content_area = $TextureRect/ContentArea/VBoxContainer/content

onready var play_stop_button = $TextureRect/ContentArea/VBoxContainer/Footer/playstopbutton
onready var pagination_buttons = $TextureRect/ContentArea/VBoxContainer/Footer/Paginations
onready var start_button = $TextureRect/ContentArea/VBoxContainer/Footer/nextButton

onready var confirmation = $ConfirmationDialog

var page_index = -1

var section_buttons = []

var current_content = null 

var check_training_pages = [1]

func _ready():
	current_content = $TextureRect/ContentArea/VBoxContainer/content/initialPage
	section_buttons = sections.get_children()
	play_stop_button.set_visible(false)
	pagination_buttons.set_visible(false)
	
	for button in section_buttons:
		button.disabled = true
	
func go_to_page(index):
	if index >= 0:
		pagination_buttons.set_visible(true)
		section_buttons[index].pressed = true
	else: # which means we are at initial page
		pagination_buttons.set_visible(false)
		change_content(initial_page)
	

# when section button pressed
# note that if you click next button section buttons will effected
func change_content(content):
	content_area.remove_child(current_content) # remove current page from content
	if content == null:
		return
	current_content = content.instance()
	page_index = current_content.page - 1 
	content_area.add_child(current_content)
	
	if page_index == 1:
		start_button.set_visible(true)
		start_button.find_node("Label").text = "Kontrol et"
	elif page_index >= 0:
		start_button.set_visible(false)
		start_button.find_node("Label").text = "Başlayalım"
	else:
		start_button.set_visible(true)
		start_button.find_node("Label").text = "Başlayalım"
		
		for button in section_buttons:
			button.disabled = true
	if not OS.get_name() == "HTML5":
		return
	if current_content is VideoPlayer:
		JavaScript.eval("showVideoOne()")
	else:
		JavaScript.eval("hideVideoOne()")
		


func _on_playstopbutton_toggled(button_pressed):
	if current_content == null:
		print("video player is null")
		return
	if button_pressed:
		if current_content.is_playing():
			current_content.paused = false
		else:
			current_content.play()
	else:
		current_content.paused = true

# start button will only shows in first page which page index is -1
func _on_nextButton_pressed():
	if start_button.find_node("Label").text == "Başlayalım":
		page_index += 1
		for button in section_buttons:
			button.disabled = false
		go_to_page(page_index)
	elif start_button.find_node("Label").text == "Kontrol et":
		var check_answer = true
		for i in len(current_content.get_draggables()):
			if i == current_content.get_draggables()[i].order_index:
				print("true")
			else:
				check_answer = false
		
		confirmation.set_visible(true)
		if not check_answer:
			confirmation.find_node("Label2").text = "Tekrar Deneyiniz"
		else:
			confirmation.find_node("Label2").text = "Doğru cevap"
			

func _on_prev_page_button_pressed():
	if page_index <= -1:
		return
	page_index -= 1
	go_to_page(page_index)

func _on_next_page_button_pressed():
	if page_index > len(section_buttons)-2:
		return
	page_index += 1
	go_to_page(page_index)
