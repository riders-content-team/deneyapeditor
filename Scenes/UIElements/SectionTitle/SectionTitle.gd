extends TextureButton

export(String) var title = "Page1"
export(Resource) var content = null

onready var title_label = $SectionTitle/Label

onready var button_group = preload("res://Scenes/UIElements/SectionTitle/sectionButtons.tres")

var main_scene = null

func _ready():
	group = button_group
	title_label.text = title
	main_scene = get_tree().get_root().find_node("MainPage", true, false)

func _on_SectionTitleButton_toggled(button_pressed):
	if button_pressed:
		main_scene.change_content(content)
