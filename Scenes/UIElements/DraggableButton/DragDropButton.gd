extends TextureRect

export(String, "Drag Button", "Drop Button") var button_type = "Drag Button"
export(int) var true_order = -1
export(String) var title = "Draggable"

onready var label = $Label

# Called when the node enters the scene tree for the first time.
func _ready():
	label.text = title

func get_drag_data(position):
	if button_type == "Drop Button":
		return
	var data = {}
	data["button"] = self
	data["button_texture"] = texture
	data["button_text"] = label.text
	data["button_true_value"] = true_order
	data["button_type"] = button_type
	
	var drag_texture = duplicate(true)
	var control = Control.new()
	control.add_child(drag_texture)
	drag_texture.rect_position = -0.5 * drag_texture.rect_size
	
	set_drag_preview(control)
	
	return data

func can_drop_data(position, data):
#	if button_type == "Drop Button" and data["button_type"] == "Drag Button":
#		if data["button_true_value"] == true_order:
#			return true
#		else:
#			return false
#	else:
#		return false
	return true

func drop_data(position, data):
	if button_type == "Drop Button" and data["button_type"] == "Drag Button":
		if data["button_true_value"] == true_order:
			label.text = data["button_text"]
		else:
			return
	else:
		return
	
