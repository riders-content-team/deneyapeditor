extends TextureRect

export(int) var order_index = -1
export(String) var title = "Draggable"

onready var label = $Label

# Called when the node enters the scene tree for the first time.
func _ready():
	label.text = title

func get_drag_data(position):
	var data = {}
	data["origin_texture"] = texture
	data["current_index"] = get_index()
	data["oreder_index"] = order_index
	data["element"] = self
	
	var drag_texture = duplicate(true)
	var control = Control.new()
	control.add_child(drag_texture)
	drag_texture.rect_position = -0.5 * drag_texture.rect_size
	
	set_drag_preview(control)
	
	return data

func can_drop_data(position, data):
	return true
#	return false

func drop_data(position, data):
	texture = data["origin_texture"]
	var dragging_element = data["element"]
	get_parent().move_child(dragging_element, get_index())
		
